package com.database;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(schema = "online_store",name = "products")
public class Product implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    @ManyToOne
    @JoinColumn(name = "id_cart")
    private Cart cart;
    @ManyToOne
    @JoinColumn(name = "id_member")
    private Member member;
    @Column(name = "id_product")
    private int idProduct;
    @Column(name = "product_name")
    private String productName;
    @Column(name = "product_price")
    private double productPrice;
    @Column(name = "stock")
    private int stock;
    @Column(name = "description")
    private String description;
    @OneToMany(mappedBy = "product")
    Set<Comment> comments = new HashSet<Comment>();

    public Product(String productName, double productPrice, int stock) {
        this.productName = productName;
        this.productPrice = productPrice;
        this.stock = stock;
    }

    public Product() {

    }

    public Cart getCart() {
        return cart;
    }

    public Product setCart(Cart cart) {
        this.cart = cart;
        return this;
    }

    public Member getMember() {
        return member;
    }

    public Product setMember(Member member) {
        this.member = member;
        return this;
    }

    public int getIdProduct() {
        return idProduct;
    }

    public Product setIdProduct(int idProduct) {
        this.idProduct = idProduct;
        return this;
    }

    public String getProductName() {
        return productName;
    }

    public Product setProductName(String productName) {
        this.productName = productName;
        return this;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public Product setProductPrice(double productPrice) {
        this.productPrice = productPrice;
        return this;
    }

    public int getStock() {
        return stock;
    }

    public Product setStock(int stock) {
        this.stock = stock;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Product setDescription(String description) {
        this.description = description;
        return this;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public Product setComments(Set<Comment> comments) {
        this.comments = comments;
        return this;
    }
}
