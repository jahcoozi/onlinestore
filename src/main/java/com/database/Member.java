package com.database;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(schema = "online_store",name = "member")
public class Member implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "user_name")
    private String userName;
    @Column(name = "user_password")
    private String password;
    @Column(name = "user_email")
    private String email;
    @Column(name = "user_full_address")
    private String userFullAdress;
    @Column(name = "user_phone")
    private String phone;
    @OneToMany(mappedBy = "member")
    private Set<Product> items = new HashSet<Product>();
    @OneToMany(mappedBy = "member")
    private Set<Comment> comments = new HashSet<Comment>();
    @OneToOne
    @JoinColumn(name = "id_transaction")
    private Transaction transaction;
    @OneToOne
    @JoinColumn(name = "id_cart")
    private Cart cart;


    public Member(String userName, String password, String email, String userFullAdress, String phone ) {
        this.userName = userName;
        this.password = password;
        this.email = email;
        this.userFullAdress = userFullAdress;
        this.phone = phone;

    }

    public Member() {

    }

//    public int getIdMember() {
//        return idMember;
//    }
//
//    public Member setIdMember(int idMember) {
//        this.idMember = idMember;
//        return this;
//    }

    public String getUserName() {
        return userName;
    }

    public Member setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public Member setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Member setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getUserFullAdress() {
        return userFullAdress;
    }

    public Member setUserFullAdress(String userFullAdress) {
        this.userFullAdress = userFullAdress;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public Member setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public Set<Product> getItems() {
        return items;
    }

    public Member setItems(Set<Product> items) {
        this.items = items;
        return this;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public Member setComments(Set<Comment> comments) {
        this.comments = comments;
        return this;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public Member setTransaction(Transaction transaction) {
        this.transaction = transaction;
        return this;
    }

    public Cart getCart() {
        return cart;
    }

    public Member setCart(Cart cart) {
        this.cart = cart;
        return this;
    }
}
