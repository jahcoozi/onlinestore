package com.database;


import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(schema = "online_store",name = "cart")
public class Cart implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    @Column(name = "id_cart")
    private int idCart;
    @Column(name = "quantity")
    private int quantity;
    @Column(name = "product_id")
    private int productId;
    @Column(name = "member_id")
    private int memberId;
    @Column(name = "total_items")
    int totalItems;
    @Column(name = "total_price")
    double totalPrice;
    @OneToMany(mappedBy = "cart")
    private Set<Product> items = new HashSet<Product>();
    @OneToOne(mappedBy = "cart")
    private Member member;


    public Cart(int quantity,int productId, int totalItems, double totalPrice) {
        this.quantity = quantity;
        this.productId = productId;
        this.totalItems = totalItems;
        this.totalPrice = totalPrice;

    }

    public Cart() {

    }

    public int getIdCart() {
        return idCart;
    }

    public Cart setIdCart(int idCart) {
        this.idCart = idCart;
        return this;
    }

    public int getQuantity() {
        return quantity;
    }

    public Cart setQuantity(int quantity) {
        this.quantity = quantity;
        return this;
    }

    public int getProductId() {
        return productId;
    }

    public Cart setProductId(int productId) {
        this.productId = productId;
        return this;
    }

    public int getMemberId() {
        return memberId;
    }

    public Cart setMemberId(int memberId) {
        this.memberId = memberId;
        return this;
    }

    public int getTotalItems() {
        return totalItems;
    }

    public Cart setTotalItems(int totalItems) {
        this.totalItems = totalItems;
        return this;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public Cart setTotalPrice(long totalPrice) {
        this.totalPrice = totalPrice;
        return this;
    }

    public Set<Product> getItems() {
        return items;
    }

    public Cart setItems(Set<Product> items) {
        this.items = items;
        return this;
    }

    public Member getMember() {
        return member;
    }

    public Cart setMember(Member member) {
        this.member = member;
        return this;
    }
}

