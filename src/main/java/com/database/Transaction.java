package com.database;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(schema = "online_store",name = "transaction")
public class Transaction implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    @Column(name = "id_transaction")
    private int transaction;
    @Column(name = "member_id")
    private int memberId;
    @Column(name = "product_id")
    private int productId;
    @Column(name = "quantity")
    private int quantity;
    @OneToOne(mappedBy = "cart")
    private Member member;

    public Transaction(int memberId, int productId, int quantity) {
        this.memberId = memberId;
        this.productId = productId;
        this.quantity = quantity;
    }

    public Transaction() {

    }

    public int getTransaction() {
        return transaction;
    }

    public Transaction setTransaction(int transaction) {
        this.transaction = transaction;
        return this;
    }

    public int getMemberId() {
        return memberId;
    }

    public Transaction setMemberId(int memberId) {
        this.memberId = memberId;
        return this;
    }

    public int getProductId() {
        return productId;
    }

    public Transaction setProductId(int productId) {
        this.productId = productId;
        return this;
    }

    public int getQuantity() {
        return quantity;
    }

    public Transaction setQuantity(int quantity) {
        this.quantity = quantity;
        return this;
    }

    public Member getMember() {
        return member;
    }

    public Transaction setMember(Member member) {
        this.member = member;
        return this;
    }
}
