package com.database;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(schema = "online_store",name = "comment")
public class Comment implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    @ManyToOne
    @JoinColumn(name = "id_product")
    private Product product;
    @ManyToOne
    @JoinColumn(name = "id_member")
    private Member member;
    @Column(name = "id_comment")
    private int comment;
    @Column(name = "member_id")
    private int memberId;
    @Column(name = "product_id")
    private int productId;
    @Column(name = "comments")
    private String  comments;

    public Comment(int comment, int memberId, int productId, String comments) {
        this.comment = comment;
        this.memberId = memberId;
        this.productId = productId;
        this.comments = comments;
    }

    public Comment() {

    }

    public Product getProduct() {
        return product;
    }

    public Comment setProduct(Product product) {
        this.product = product;
        return this;
    }

    public Member getMember() {
        return member;
    }

    public Comment setMember(Member cMember) {
        this.member = member;
        return this;
    }

    public int getComment() {
        return comment;
    }

    public Comment setComment(int comment) {
        this.comment = comment;
        return this;
    }

    public int getMemberId() {
        return memberId;
    }

    public Comment setMemberId(int memberId) {
        this.memberId = memberId;
        return this;
    }

    public int getProductId() {
        return productId;
    }

    public Comment setProductId(int productId) {
        this.productId = productId;
        return this;
    }

    public String getComments() {
        return comments;
    }

    public Comment setComments(String comments) {
        this.comments = comments;
        return this;
    }
}
