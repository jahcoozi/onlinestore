package com.dao;


import com.database.Transaction;

public interface TransactionDao {
    Transaction createTransaction(Transaction transaction);
    void deleteTransaction(Transaction transaction, long id);
    Transaction getTransaction(long id);
    Transaction updateTransaction(Transaction transaction, long id);
}
