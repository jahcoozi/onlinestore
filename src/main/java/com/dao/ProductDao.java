package com.dao;


import com.database.Product;

public interface ProductDao {
    Product createProduct(Product product);
    void deleteProduct(Product product, long id);
    Product getProduct(long id);
    Product updateProduct(Product product, long id);
}
