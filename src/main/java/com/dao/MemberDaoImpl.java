package com.dao;


import com.database.Member;
import com.database.Utils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class MemberDaoImpl implements MemberDao {
    SessionFactory sessionFactory = Utils.getSessionFactory();
    Member retrievedMember;
    public Member createMember(Member member) {
        Session session = this.sessionFactory.openSession();
        session.save(member);
        session.close();


        return member;
    }

    public void deleteMember(Member member, long id) {
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        this.retrievedMember = session.get(Member.class, id);
        session.remove(retrievedMember);
        transaction.commit();
        session.close();
    }

    public Member getMember(long id) {
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        this.retrievedMember = session.get(Member.class, id);
        transaction.commit();
        session.close();
        return retrievedMember;
    }

    public Member updateMember(Member member, long id) {
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        this.retrievedMember = session.get(Member.class, id);
        this.retrievedMember.setUserName(member.getUserName());

        session.update(retrievedMember);
        transaction.commit();
        session.close();
        return retrievedMember;
    }
}
