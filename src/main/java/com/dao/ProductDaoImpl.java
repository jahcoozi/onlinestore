package com.dao;


import com.database.Product;
import com.database.Utils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;


public class ProductDaoImpl implements ProductDao {
    SessionFactory sessionFactory = Utils.getSessionFactory();
    Product retrievedProduct;

    public Product createProduct(Product product) {
        Session session = this.sessionFactory.openSession();
        session.save(product);
        session.close();
        return product;
    }

    public void deleteProduct(Product product, long id) {
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        this.retrievedProduct = session.get(Product.class, id);
        session.remove(retrievedProduct);
        transaction.commit();
        session.close();
    }

    public Product getProduct(long id) {
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        this.retrievedProduct = session.get(Product.class, id);
        transaction.commit();
        session.close();
        return retrievedProduct;
    }

    public Product updateProduct(Product product, long id) {
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        this.retrievedProduct = session.get(Product.class, id);
        this.retrievedProduct.setProductName(product.getProductName()).setProductPrice(product.getProductPrice()).setStock(product.getStock());

        session.update(retrievedProduct);
        transaction.commit();
        session.close();
        return retrievedProduct;
    }
}
