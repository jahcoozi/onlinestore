package com.dao;

import com.database.Cart;
import com.database.Utils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class CartDaoImpl implements CartDao {

    SessionFactory sessionFactory = Utils.getSessionFactory();
    Cart retrievedCart;

    public CartDaoImpl() {

    }



    public Cart createCart(Cart cart) {
        Session session = this.sessionFactory.openSession();
        session.save(cart);
        session.close();


        return cart;
    }

    public void deleteCart(Cart cart, long id) {
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        this.retrievedCart = session.get(Cart.class, id);
        session.remove(retrievedCart);
        transaction.commit();
        session.close();
    }

    public Cart getCart(long id) {
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        this.retrievedCart = session.get(Cart.class, id);
        transaction.commit();
        session.close();
        return retrievedCart;

    }

    public Cart updateCart(Cart cart, long id) {

        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        this.retrievedCart = session.get(Cart.class, id);
        this.retrievedCart.setItems(cart.getItems()).setQuantity(cart.getQuantity());

        session.update(retrievedCart);
        transaction.commit();
        session.close();
        return retrievedCart;

    }
}
