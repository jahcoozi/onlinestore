package com.dao;

import com.database.Comment;
import com.database.Utils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class CommentDaoImpl implements CommentDao {


    SessionFactory sessionFactory = Utils.getSessionFactory();
    Comment retrievedComment;

    public Comment createComment(Comment comment) {
        Session session = this.sessionFactory.openSession();
        session.save(comment);
        session.close();


        return comment;
    }

    public void deleteComment(Comment comment, long id) {
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        this.retrievedComment = session.get(Comment.class, id);
        session.remove(retrievedComment);
        transaction.commit();
        session.close();
    }

    public Comment getComment(long id) {
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        this.retrievedComment = session.get(Comment.class, id);
        transaction.commit();
        session.close();
        return retrievedComment;
    }

    public Comment updateComment(Comment comment, long id) {
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        this.retrievedComment = session.get(Comment.class, id);
        this.retrievedComment.setComment(comment.getComment()).setComments(comment.getComments());

        session.update(retrievedComment);
        transaction.commit();
        session.close();
        return retrievedComment;

    }
}
