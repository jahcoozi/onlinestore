package com.dao;


import com.database.Comment;

public interface CommentDao {
    Comment createComment(Comment comment);
    void deleteComment(Comment comment, long id);
    Comment getComment(long id);
    Comment updateComment(Comment comment, long id);
}
