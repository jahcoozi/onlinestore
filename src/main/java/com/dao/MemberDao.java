package com.dao;


import com.database.Member;

public interface MemberDao {
    Member createMember(Member member);
    void deleteMember(Member member, long id);
    Member getMember(long id);
    Member updateMember(Member member, long id);
}
