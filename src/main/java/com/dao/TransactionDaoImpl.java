package com.dao;




import com.database.Transaction;
import com.database.Utils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class TransactionDaoImpl implements TransactionDao {
    SessionFactory sessionFactory = Utils.getSessionFactory();
    Transaction retrievedTransaction;
    public Transaction createTransaction(Transaction transaction) {
        Session session = this.sessionFactory.openSession();
        session.save(transaction);
        session.close();


        return transaction;
    }

    public void deleteTransaction(Transaction transaction, long id) {
        Session session = this.sessionFactory.openSession();
        org.hibernate.Transaction trans = session.beginTransaction();
        this.retrievedTransaction = session.get(Transaction.class, id);
        session.remove(retrievedTransaction);
        trans.commit();
        session.close();
    }

    public Transaction getTransaction(long id) {
        Session session = this.sessionFactory.openSession();
        org.hibernate.Transaction trans = session.beginTransaction();
        this.retrievedTransaction = session.get(Transaction.class, id);
        trans.commit();
        session.close();
        return retrievedTransaction;
    }

    public Transaction updateTransaction(Transaction transaction, long id) {
        Session session = this.sessionFactory.openSession();
        org.hibernate.Transaction trans = session.beginTransaction();
        this.retrievedTransaction = session.get(Transaction.class, id);
        this.retrievedTransaction.setQuantity(transaction.getQuantity());

        session.update(retrievedTransaction);
        trans.commit();
        session.close();
        return retrievedTransaction;
    }
}
