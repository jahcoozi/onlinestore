package com.dao;

import com.database.Cart;

public interface CartDao {
    Cart createCart(Cart cart);
    void deleteCart(Cart cart, long id);
    Cart getCart(long id);
    Cart updateCart(Cart cart, long id);
}
